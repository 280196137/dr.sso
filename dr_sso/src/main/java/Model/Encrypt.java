package Model;

/// <summary>
/// 非对称加密秘钥
/// </summary>
public class Encrypt
{
    /// <summary>
    /// 用户ID
    /// </summary>
    public int UserId ;
    /// <summary>
    /// 对外秘钥
    /// </summary>
    public String Key ;
    /// <summary>
    /// 对内秘钥
    /// </summary>
    public String Value ;
}