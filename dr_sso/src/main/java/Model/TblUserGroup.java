package Model;

/// <summary>
///用户组
/// </summary>
public class TblUserGroup
{
    /// <summary>
    /// 组id
    /// </summary>
    public int GroupId ;
    /// <summary>
    /// 组名称
    /// </summary>
    public String GroupName ;
    /// <summary>
    /// 状态
    /// </summary>
    public int State ;
}