package Model;

/// <summary>
/// 角色权限
/// </summary>
public class TblRolePower
{
    /// <summary>
    /// 编号
    /// </summary>
    public int Id ;
    /// <summary>
    /// 角色ID
    /// </summary>
    public int RoleId ;
    /// <summary>
    /// 权限ID
    /// </summary>
    public int PowerId ;
}
