package Model;

/// <summary>
///角色表
/// </summary>
public class TblRole
{
    /// <summary>
    ///角色id
    /// </summary>
    public int RoleId ;
    /// <summary>
    /// 角色名称
    /// </summary>
    public String RoleName ;
    /// <summary>
    /// 角色状态
    /// </summary>
    public int IsLock ;

}
