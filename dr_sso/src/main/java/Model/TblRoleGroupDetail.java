package Model;

/// <summary>
/// 角色组详情
/// </summary>
public class TblRoleGroupDetail
{
    /// <summary>
    /// 编号
    /// </summary>
    public int Id ;
    /// <summary>
    /// 组Id
    /// </summary>
    public int GroupId ;
    /// <summary>
    /// 角色Id
    /// </summary>
    public int RoleId ;
}
