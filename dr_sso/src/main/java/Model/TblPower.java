package Model;

/// <summary>
/// 权限表
/// </summary>
public class TblPower
{
    /// <summary>
    /// 权限ID
    /// </summary>
    public int PowerId ;
    /// <summary>
    /// 权限名称
    /// </summary>
    public String PowerName ;
    /// <summary>
    /// 权限状态
    /// </summary>;{ get; set; }
}