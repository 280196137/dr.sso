package Model;

/// <summary>
/// 用户角色表
/// </summary>
public  class TblUserRole
{
    /// <summary>
    /// 编号
    /// </summary>
    public int Id ;
    /// <summary>
    /// 用户Id
    /// </summary>
    public int UserId;
    /// <summary>
    /// 角色Id
    /// </summary>
    public int RoleId ;
    /// <summary>
    /// 角色顺序(默认角色)
    /// </summary>
    public int IndexOf ;
}