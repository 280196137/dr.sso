package Model;

/// <summary>
/// 用户组详情
/// </summary>
public  class TblUserGroupDetail
{
    /// <summary>
    /// 编号
    /// </summary>
    public int Id ;
    /// <summary>
    /// 组Id
    /// </summary>
    public int GroupId ;
    /// <summary>
    /// 用户Id
    /// </summary>
    public int UserId ;
}