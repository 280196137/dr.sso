package Model;
/// <summary>
/// 用户表
/// </summary>
public class TblUser
{
    /// <summary>
    /// 用户ID
    /// </summary>
    public int UserId ;
    /// <summary>
    /// 用户名
    /// </summary>
    public int UserName ;
    /// <summary>
    /// 用户编号
    /// </summary>
    public int UserCode ;
    /// <summary>
    /// 用户电话
    /// </summary>
    public int UserPhone ;
    /// <summary>
    /// 用户电话
    /// </summary>
    public int UserWeChat ;
    /// <summary>
    /// 用户密码
    /// </summary>
    public int UserPassword ;
    /// <summary>
    /// 用户添加时间
    /// </summary>
    public int UserCreateDate ;
    /// <summary>
    /// 用户修改时间
    /// </summary>
    public int UserUpdateDate ;
    /// <summary>
    /// 用户状态
    /// </summary>
    public int IsLock ;
}
