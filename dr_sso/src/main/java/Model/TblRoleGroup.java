package Model;

/// <summary>
/// 角色组
/// </summary>
public class TblRoleGroup
{
    /// <summary>
    /// 组Id
    /// </summary>
    public int GroupId ;
    /// <summary>
    /// 组名称
    /// </summary>
    public String GroupName ;
    /// <summary>
    /// 状态
    /// </summary>
    public int State ;
}
