package com.sso.controller;

import Model.TblPower;
import Tool.ResultModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/power")
public class PowerController
{
    /// <summary>
    /// 添加权限
    /// </summary>
    /// <param name="tblPower"></param>
    /// <returns></returns>
    @RequestMapping(value="/addPower",method =RequestMethod.POST)
    public ResultModel AddPower(TblPower tblPower){return  null;}
    /// <summary>
    /// 更新权限
    /// </summary>
    /// <param name="tblPower"></param>
    /// <param name="PowerId"></param>
    /// <returns></returns>
    @RequestMapping(value="updatePower",method=RequestMethod.PUT)
    public ResultModel UpdatePower(TblPower tblPower,int PowerId){return  null;}
    /// <summary>
    /// 删除权限
    /// </summary>
    /// <param name="PowerId"></param>
    /// <returns></returns>
    @RequestMapping(value="deletePower",method = RequestMethod.DELETE)
    public ResultModel DeletePower(@RequestParam int PowerId){return  null;}

    /// <summary>
    /// 查询权限
    /// </summary>
    /// <returns></returns>
    @RequestMapping("/selectPower")
    public ResultModel SelectPower(){return  null;}
}
