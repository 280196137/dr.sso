package com.example.dr_sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.sso.*")
public class DrSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DrSsoApplication.class, args);
    }

}
