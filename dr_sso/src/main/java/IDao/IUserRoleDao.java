package IDao;
import Model.*;
import java.util.List;
/// <summary>
/// 用户角色
/// </summary>
public interface IUserRoleDao
{
    /// <summary>
    /// 添加用户角色
    /// </summary>
    /// <param name="tblUserRole"></param>
    /// <returns></returns>
    int AddUserRoleTran(TblUserRole tblUserRole);
    /// <summary>
    /// 删除用户角色
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    int DeleteUserRoleTran(int UserId, int RoleId);
    /// <summary>
    /// 获取用户角色
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    List<TblUserRole> SelectUserRoleByUserId(int UserId);
    /// <summary>
    /// 获取用户角色
    /// </summary>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    List<TblUserRole> SelectUserRoleByRoleId(int RoleId);
}