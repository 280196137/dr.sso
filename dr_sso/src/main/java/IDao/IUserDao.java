package IDao;

import Model.*;
public interface IUserDao
{
    /// <summary>
    /// 添加用户
    /// </summary>
    /// <param name="tblUser"></param>
    /// <returns></returns>
    int AddUser(TblUser tblUser);
    /// <summary>
    /// 修改用户
    /// </summary>
    /// <param name="tblUser"></param>
    /// <returns></returns>
    int UpdateUser(TblUser tblUser, int UserId);
    /// <summary>
    /// 删除用户
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    int DeleteUser(int UserId);
    /// <summary>
    /// 获取用户
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    TblUser SelectUserByUserId(int UserId);
}
