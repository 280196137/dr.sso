package IDao;
import Model.*;
import java.util.List;

/// <summary>
/// 用户组详情
/// </summary>
public  interface IUserGroupDetailDao
{
    /// <summary>
    /// 添加用户组详情
    /// </summary>
    /// <param name="tblUserGroupDetail"></param>
    /// <returns></returns>
    int AddUserGroupDetailTran(TblUserGroupDetail tblUserGroupDetail);
    /// <summary>
    /// 更新用户组详情
    /// </summary>
    /// <param name="tblUserGroupDetail"></param>
    /// <returns></returns>
    int UpdateUserGroupDetailTran(TblUserGroupDetail tblUserGroupDetail);
    /// <summary>
    /// 删除用户组详情
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    int DeleteUserGroupDetailTran(int UserId);
    /// <summary>
    /// 获取用户组详情
    /// </summary>
    /// <returns></returns>
    List<TblUserGroupDetail> SelectTblRoleGroupDetail();
    /// <summary>
    /// 获取用户组详情
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    List<TblUserGroupDetail> SelectTblRoleGroupDetailByUserId(int UserId);
    /// <summary>
    /// 获取用户组详情
    /// </summary>
    /// <param name="GroupId"></param>
    /// <returns></returns>
    List<TblUserGroupDetail> SelectTblRoleGroupDetailByGroupId(int GroupId);
}