package IDao;
import Model.*;
import java.util.List;
/// <summary>
///角色
/// </summary>
public interface IRoleDao
{
    /// <summary>
    /// 添加角色
    /// </summary>
    /// <param name="tblRole"></param>
    /// <returns></returns>
    int AddRole(TblRole tblRole);
    /// <summary>
    /// 删除角色
    /// </summary>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    int DeleteRole(int RoleId);
    /// <summary>
    /// 更新角色
    /// </summary>
    /// <param name="tblRole"></param>
    /// <returns></returns>
    int UpdateRole(TblRole tblRole);
    /// <summary>
    /// 获取角色
    /// </summary>
    /// <returns></returns>
    List<TblRole> SelectRole();
    /// <summary>
    /// 获取角色
    /// </summary>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    TblRole SelectRoleByRoleId(int RoleId);
}
