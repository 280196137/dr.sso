package IDao;
import Model.*;
import java.util.List;
/// <summary>
/// 用户组
/// </summary>
public  interface IUserGroupDao {
    /// <summary>
    /// 添加用户组
    /// </summary>
    /// <param name="tblUserGroup"></param>
    /// <returns></returns>
    int AddRoleGroup(TblUserGroup tblUserGroup);

    /// <summary>
    /// 删除用户组
    /// </summary>
    /// <param name="GroupId"></param>
    /// <returns></returns>
    int DeleteRoleGroup(int GroupId);

    /// <summary>
    /// 更新用户组
    /// </summary>
    /// <param name="tblUserGroup"></param>
    /// <returns></returns>
    int UpdateUserGroup(TblUserGroup tblUserGroup);

    /// <summary>
    /// 获取用户组
    /// </summary>
    /// <param name="GroupId"></param>
    /// <returns></returns>
    List<TblUserGroup> SelectTblUserGroup(int GroupId);

    /// <summary>
    /// 获取用户组
    /// </summary>
    /// <returns></returns>
    List<TblUserGroup> SelectTblUserGroup();
}
