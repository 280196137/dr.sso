package IDao;
import Model.*;
import java.util.List;

/// <summary>
/// 角色组
/// </summary>
public interface IRoleGroupDao
{
    /// <summary>
    /// 添加角色组
    /// </summary>
    /// <param name="tblRoleGroup"></param>
    /// <returns></returns>
    int AddRoleGroup(TblRoleGroup tblRoleGroup);
    /// <summary>
    /// 删除角色组
    /// </summary>
    /// <param name="GroupId"></param>
    /// <returns></returns>
    int DeleteRoleGroupByGroupId(int GroupId);
    /// <summary>
    /// 更新角色组
    /// </summary>
    /// <param name="tblRoleGroup"></param>
    /// <returns></returns>
    int UpdateRoleGroup(TblRoleGroup tblRoleGroup);
    /// <summary>
    /// 获取组
    /// </summary>
    /// <param name="GroupId"></param>
    /// <returns></returns>
    List<TblRoleGroup> SelectTblRoleGroup(int GroupId);
    /// <summary>
    /// 获取组
    /// </summary>
    /// <returns></returns>
    List<TblRoleGroup> SelectTblRoleGroup();
}
