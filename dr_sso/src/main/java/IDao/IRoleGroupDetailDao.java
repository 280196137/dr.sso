package IDao;
import Model.*;
import java.util.List;
/// <summary>
/// 角色组详情
/// </summary>
public interface IRoleGroupDetailDao
{
    /// <summary>
    /// 添加角色组详情
    /// </summary>
    /// <param name="tblRoleGroupDetail"></param>
    /// <returns></returns>
    int AddRoleGroupDetailTran(TblRoleGroupDetail tblRoleGroupDetail);
    /// <summary>
    /// 更新角色组详情
    /// </summary>
    /// <param name="tblRoleGroupDetail"></param>
    /// <returns></returns>
    int UpdateRoleGroupDetailTran(TblRoleGroupDetail tblRoleGroupDetail);
    /// <summary>
    ///删除角色组详情
    /// </summary>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    int DeleteRoleGroupDetailTran(int RoleId);
    /// <summary>
    /// 获取角色组详情
    /// </summary>
    /// <returns></returns>
    List<TblRoleGroupDetail> SelectTblRoleGroupDetail();
    /// <summary>
    /// 获取角色组详情
    /// </summary>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    List<TblRoleGroupDetail> SelectTblRoleGroupDetailByRoleId(int RoleId);
    /// <summary>
    /// 获取角色组详情
    /// </summary>
    /// <param name="GroupId"></param>
    /// <returns></returns>
    List<TblRoleGroupDetail> SelectTblRoleGroupDetailByGroupId(int GroupId);
}
