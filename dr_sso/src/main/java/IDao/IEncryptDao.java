package IDao;
import java.util.List;
import Model.*;

import java.util.List;

/// <summary>
/// 非对称加密
/// </summary>
public interface IEncryptDao
{
    /// <summary>
    /// 添加秘钥
    /// </summary>
    /// <param name="encrypt"></param>
    /// <returns></returns>
    int AddEncrypt(Encrypt encrypt);
    /// <summary>
    /// 获取秘钥
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="Key"></param>
    /// <returns></returns>
    Encrypt SelectEncrypt(int UserId,String Key);
    /// <summary>
    /// 获取全部秘钥
    /// </summary>
    /// <returns></returns>
    List<Encrypt> SelectEncrypt();
    /// <summary>
    /// 删除秘钥
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    int DeleteEncrypt(int UserId);
    /// <summary>
    /// 更新秘钥
    /// </summary>
    /// <param name="encrypt"></param>
    /// <returns></returns>
    int UpdateEncrypt(Encrypt encrypt);

}