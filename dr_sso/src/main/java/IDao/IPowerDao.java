package IDao;
import Model.*;
import java.util.List;
/// <summary>
/// 权限
/// </summary>
public interface IPowerDao
{
    /// <summary>
    /// 添加权限
    /// </summary>
    /// <param name="tblPower"></param>
    /// <returns></returns>
    int AddPower(TblPower tblPower);
    /// <summary>
    /// 更新权限
    /// </summary>
    /// <param name="tblPower"></param>
    /// <param name="PowerId"></param>
    /// <returns></returns>
    int UpdatePower(TblPower tblPower,int PowerId);
    /// <summary>
    /// 删除权限
    /// </summary>
    /// <param name="PowerId"></param>
    /// <returns></returns>
    int DeletePower(int PowerId);

    /// <summary>
    /// 查询权限
    /// </summary>
    /// <returns></returns>
    List<TblPower> SelectPower();
}