package IDao;
import Model.*;
import java.util.List;
/// <summary>
/// 角色权限
/// </summary>
public   interface IRolePowerDao
{
    /// <summary>
    /// 添加角色权限
    /// </summary>
    /// <param name="tblRolePower"></param>
    /// <returns></returns>
    int AddRolePowerTran(TblRolePower tblRolePower);
    /// <summary>
    /// 删除角色权限
    /// </summary>
    /// <param name="RoleId"></param>
    /// <param name="PowerId"></param>
    /// <returns></returns>
    int DeleteRolePowerTran(int RoleId, int PowerId);
    /// <summary>
    /// 删除角色权限
    /// </summary>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    int DeleteRolePowerTranByRoleId(int RoleId);
    /// <summary>
    /// 删除角色权限
    /// </summary>
    /// <param name="PowerId"></param>
    /// <returns></returns>
    int DeleteRolePowerTranByPowerId(int PowerId);
    /// <summary>
    /// 获取角色权限
    /// </summary>
    /// <param name="RoleId"></param>
    /// <returns></returns>
    List<TblRolePower> SelectRolePowerByRoleId(int RoleId);
    /// <summary>
    /// 获取角色权限
    /// </summary>
    /// <param name="PowerId"></param>
    /// <returns></returns>
   List<TblRolePower> SelectRolePowerByPowerId(int PowerId);
}
