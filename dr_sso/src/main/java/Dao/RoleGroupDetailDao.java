package Dao;
import IDao.IRoleGroupDetailDao;
import Model.TblRoleGroupDetail;

import java.util.List;

public class RoleGroupDetailDao implements  IRoleGroupDetailDao
{
    @Override
    public int AddRoleGroupDetailTran(TblRoleGroupDetail tblRoleGroupDetail) {
        return 0;
    }

    @Override
    public int UpdateRoleGroupDetailTran(TblRoleGroupDetail tblRoleGroupDetail) {
        return 0;
    }

    @Override
    public int DeleteRoleGroupDetailTran(int RoleId) {
        return 0;
    }

    @Override
    public List<TblRoleGroupDetail> SelectTblRoleGroupDetail() {
        return null;
    }

    @Override
    public List<TblRoleGroupDetail> SelectTblRoleGroupDetailByRoleId(int RoleId) {
        return null;
    }

    @Override
    public List<TblRoleGroupDetail> SelectTblRoleGroupDetailByGroupId(int GroupId) {
        return null;
    }
}
