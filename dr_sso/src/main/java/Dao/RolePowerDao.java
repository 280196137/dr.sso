package Dao;
import IDao.IRolePowerDao;
import Model.TblRolePower;

import java.util.List;

public class RolePowerDao implements IRolePowerDao
{

    @Override
    public int AddRolePowerTran(TblRolePower tblRolePower) {
        return 0;
    }

    @Override
    public int DeleteRolePowerTran(int RoleId, int PowerId) {
        return 0;
    }

    @Override
    public int DeleteRolePowerTranByRoleId(int RoleId) {
        return 0;
    }

    @Override
    public int DeleteRolePowerTranByPowerId(int PowerId) {
        return 0;
    }

    @Override
    public List<TblRolePower> SelectRolePowerByRoleId(int RoleId) {
        return null;
    }

    @Override
    public List<TblRolePower> SelectRolePowerByPowerId(int PowerId) {
        return null;
    }
}
