package Dao;
import IDao.IRoleGroupDao;
import Model.TblRoleGroup;

import java.util.List;

public class RoleGroupDao implements IRoleGroupDao
{

    @Override
    public int AddRoleGroup(TblRoleGroup tblRoleGroup) {
        return 0;
    }

    @Override
    public int DeleteRoleGroupByGroupId(int GroupId) {
        return 0;
    }

    @Override
    public int UpdateRoleGroup(TblRoleGroup tblRoleGroup) {
        return 0;
    }

    @Override
    public List<TblRoleGroup> SelectTblRoleGroup(int GroupId) {
        return null;
    }

    @Override
    public List<TblRoleGroup> SelectTblRoleGroup() {
        return null;
    }
}
