package Dao;
import IDao.IUserGroupDetailDao;
import Model.TblUserGroupDetail;

import java.util.List;

public class UserGroupDetailDao implements  IUserGroupDetailDao
{

    @Override
    public int AddUserGroupDetailTran(TblUserGroupDetail tblUserGroupDetail) {
        return 0;
    }

    @Override
    public int UpdateUserGroupDetailTran(TblUserGroupDetail tblUserGroupDetail) {
        return 0;
    }

    @Override
    public int DeleteUserGroupDetailTran(int UserId) {
        return 0;
    }

    @Override
    public List<TblUserGroupDetail> SelectTblRoleGroupDetail() {
        return null;
    }

    @Override
    public List<TblUserGroupDetail> SelectTblRoleGroupDetailByUserId(int UserId) {
        return null;
    }

    @Override
    public List<TblUserGroupDetail> SelectTblRoleGroupDetailByGroupId(int GroupId) {
        return null;
    }
}
