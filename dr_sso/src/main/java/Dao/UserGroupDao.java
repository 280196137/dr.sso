package Dao;
import IDao.IUserGroupDao;
import Model.TblUserGroup;

import java.util.List;

public class UserGroupDao implements  IUserGroupDao
{

    @Override
    public int AddRoleGroup(TblUserGroup tblUserGroup) {
        return 0;
    }

    @Override
    public int DeleteRoleGroup(int GroupId) {
        return 0;
    }

    @Override
    public int UpdateUserGroup(TblUserGroup tblUserGroup) {
        return 0;
    }

    @Override
    public List<TblUserGroup> SelectTblUserGroup(int GroupId) {
        return null;
    }

    @Override
    public List<TblUserGroup> SelectTblUserGroup() {
        return null;
    }
}
