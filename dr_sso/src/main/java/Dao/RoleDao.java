package Dao;
import  IDao.IRoleDao;
import Model.TblRole;

import java.util.List;

public class RoleDao implements IRoleDao
{


    @Override
    public int AddRole(TblRole tblRole) {
        return 0;
    }

    @Override
    public int DeleteRole(int RoleId) {
        return 0;
    }

    @Override
    public int UpdateRole(TblRole tblRole) {
        return 0;
    }

    @Override
    public List<TblRole> SelectRole() {
        return null;
    }

    @Override
    public TblRole SelectRoleByRoleId(int RoleId) {
        return null;
    }
}
