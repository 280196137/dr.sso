package Dao;
import IDao.IUserRoleDao;
import Model.TblUserRole;

import java.util.List;

public class UserRoleDao implements IUserRoleDao
{

    @Override
    public int AddUserRoleTran(TblUserRole tblUserRole) {
        return 0;
    }

    @Override
    public int DeleteUserRoleTran(int UserId, int RoleId) {
        return 0;
    }

    @Override
    public List<TblUserRole> SelectUserRoleByUserId(int UserId) {
        return null;
    }

    @Override
    public List<TblUserRole> SelectUserRoleByRoleId(int RoleId) {
        return null;
    }
}
