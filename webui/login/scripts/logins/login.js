﻿var clikeUrl = null;
var discipType = "";
$(function () {

    var UserAccountId = localStorage.getItem("UserAccountId");
    var UserPassWord = localStorage.getItem("UserPassWord");

    var Time = localStorage.getItem("Time");
    var url = window.location.href;
    clikeUrl = GetArgsFromHref(url, "clikeurl");
    discipType = GetArgsFromHref(url, "disciptype");
    if (clikeUrl=="") {
        clikeUrl = GetFromHref(url, "clikeurl");
    }
    if (discipType =="") {
        discipType = GetFromHref(url, "disciptype");
        if (discipType!="") {
            discipType = discipType.split('?')[0];
        }
    }

    if (UserAccountId && UserPassWord) {
        var time = dateDiff(jsonDateFormat(new Date(), "yyyy-MM-dd"), Time);
        if (time <= 30) {

        } else {
            localStorage.removeItem("UserAccountId");
            localStorage.removeItem("UserPassWord");
            localStorage.removeItem("Time");
            alert("密码已过期，请重新登录")
        }
    }

});

function dateDiff(date1, date2) {
    var type1 = typeof date1, type2 = typeof date2;
    if (type1 == 'string')
        date1 = stringToTime(date1);
    else if (date1.getTime)
        date1 = date1.getTime();
    if (type2 == 'string')
        date2 = stringToTime(date2);
    else if (date2.getTime)
        date2 = date2.getTime();
    return (date1 - date2) / (1000 * 60 * 60 * 24); //结果是小时 
}

//字符串转成Time(dateDiff)所需方法 
function stringToTime(string) {
    var f = string.split(' ', 2);
    var d = (f[0] ? f[0] : '').split('-', 3);
    var t = (f[1] ? f[1] : '').split(':', 3);
    return (new Date(
   parseInt(d[0], 10) || null,
   (parseInt(d[1], 10) || 1) - 1,
    parseInt(d[2], 10) || null,
    parseInt(t[0], 10) || null,
    parseInt(t[1], 10) || null,
    parseInt(t[2], 10) || null
    )).getTime();
}


function GetPost() {
    var option = {};
    option.UserAccountId = $("#UserAccountId").val();
    option.UserAccountPassWord = $("#UserAccountPassWord").val();
    option.CourseId = discipType;
    option.clikeUrl = clikeUrl;
    return option;
}
var booleans = false;
//登录
function Login() {
    var url = "/api/UserAccount/GetUserLogin";
    var cbk = $("#remmberpwd").prop("checked");
    var issh = $("input[name=categery]:checked").val();
    if (discipType=="") {
        discipType = disciptype;
    }
    if ($("#UserAccountId").val() == "" || $("#UserAccountId").val() == null) {
        alert("请输入账户");
        return false;
    }
    if ($("#UserAccountPassWord").val() == "" || $("#UserAccountPassWord").val() == null) {
        alert("请输入密码");
        return false;
    }
    if (booleans == false) {
        alert("请滑动验证");
        return false;
    }
    var data = jsonPost(url, GetPost());
    if (data) {
        var datas = JSON.parse(data);
        if (datas.Result == 1) {
            sessionStorage.setItem("UserId", datas.ID);//用户加密账号
            if (datas.Data) {
                var model = JSON.parse(datas.Data);
                sessionStorage.setItem("identity", model.UserAccountEnum);//登录身份
            }
            if (window.localStorage) {
                if (cbk) {
                    localStorage.setItem("UserAccountId", $("#UserAccountId").val());
                    localStorage.setItem("UserPassWord", $("#UserAccountPassWord").val());
                    localStorage.setItem("Time", jsonDateFormat(new Date(), "yyyy-MM-dd"));
                }
            } else {
                $("#cbk").prop("checked", false);
            }
            alert(datas.Message);
            datas = JSON.parse(datas.Data);
            //设置用户信息
            localStorage.setItem("userinfo", JSON.stringify(datas));
            var token = "?usertoken=" + datas.UserToken + "&school=" + datas.UserAccountSchoolId + "&class=" + datas.ClassId + "&grade=" + datas.GradeId + "&buytype=" + datas.BuyType + "&restime=" + datas.Restime + "&sign=" + datas.Sign;
            localStorage.setItem("tokens", token);
            if (datas.UserAccountEnum == 1) {
                window.location.href = "http://schooladmin.keys-edu.com/school/company/inde.html" + token;
                return;
            }
            //跳转之前访问页面
            if (clikeUrl != null && clikeUrl != "") {
                var url = clikeUrl + token;
                window.location.href = url;
            } else {
                window.location.href = "http://www.keys-edu.com/index.html";
            }

        } else if (datas.Result == 0) {
            alert(datas.Message);
        } else if (datas.Result == 2) {
            alert(datas.Message);
        } else {
            alert("服务器正忙！！！");
        }
    }
}



































