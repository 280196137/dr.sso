﻿var jsConfigs = function () {
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?4533a9fe14b8787fe0c7fcb63893a836";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
    var _hmt1 = _hmt1 || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?3ff42089ba5f5c90ad6da206f779c2f7";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
    //闭包
    var tempcar = new Object;
    //图片地址路由
    tempcar.imgPath = function () {
        var array = new Array();
        array.push("http://image.keys-edu.com/");
        array.push("http://image.keys-edu.com/");
        array.push("http://image.keys-edu.com/");
        var rand = Math.random(0, 1) * array.length;
        rand = parseInt(rand);
        return array[rand];
    };
    tempcar.imgAddress600X600 = tempcar.imgPath() + "/600X600/";
    tempcar.imgAddress240X240 = tempcar.imgPath() + "/240X240/";

    //audio地址    
    tempcar.audioPath = function () {
        var array = new Array();
        array.push("http://audio.keys-edu.com/");
        array.push("http://audio.keys-edu.com/");
        array.push("http://audio.keys-edu.com/");
        var rand = Math.random(0, 1) * array.length;
        rand = parseInt(rand);
        return array[rand];
    };
    //应用地址
    tempcar.path = "";
    //webapi地址    
    tempcar.webApiPath = function () {
        var array = new Array();
        array.push("http://192.168.16.37:8061/");
        //array.push("http://loginapi.keys-edu.com/");
        //array.push("http://loginapi.keys-edu.com/");
        //array.push("http://loginapi.keys-edu.com/");
        var rand = Math.random(0, 1) * array.length;
        rand = parseInt(rand);
        return array[rand];
    };
    return tempcar;
}
var webConfigs = new jsConfigs();
webConfigs.imgAddress;
webConfigs.webApiPath;
webConfigs.audioPath;
//获取传递参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}


//PostSubmit 提交 
function jsonPost(iUrl, iData) {
    var dataf;
    var datas;
    var url = webConfigs.webApiPath() + "/" + iUrl;
    $.ajax({
        type: 'POST',
        url: url,
        data: iData,
        async: false,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data.Result === undefined) {
                datas = JSON.parse(data);
                dataf = data;
            }
            else {
                dataf = JSON.stringify(data);
            }
            //if (datas.Result == 1) {
            //    alert(datas.Message);
            //    window.location.href = webConfigs.path + iReturnUrl;
            //}
            //if (datas.Result == 2) {
            //    alert(datas.Message);
            //    return;
            //}
            //if (datas.Result == 3) {
            //    alert("服务器繁忙，请稍后再试！");
            //    return;
            //}
            if (data.Result == 4 || data.Result == 3) {
                window.location.href = webConfigs.path + "/login.html";
            }
        },
        error: function (data) {
            datas = data;
        }
    });
    return dataf;
}



//#region json日期格式转换为正常格式
function jsonDateFormat(jsonDate, fmt) { //
    if (typeof (jsonDate) == "undefined" || jsonDate.length === 0) return "";
    var date;
    try {
        if ($.type(jsonDate) === "date" || $.type(jsonDate) === "object") {
            date = jsonDate;
        } else {
            if (jsonDate.indexOf("/Date(") < 0) {
                var e = new RegExp("-", "g");
                jsonDate = jsonDate.replace(e, '/');
                date = new Date(jsonDate);
            } else {
                date = new Date(parseInt(jsonDate.replace("/Date(", "").replace(")/", ""), 10));
            }
        }
        //            var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        //            var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        //            var hours = date.getHours();
        //            var minutes = date.getMinutes();
        //            var seconds = date.getSeconds();
        //            var milliseconds = date.getMilliseconds();
        //return date.getFullYear() + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds + "." + milliseconds;
        var fullyear = date.getFullYear();
        if (fullyear <= 1753) return "";
        if (typeof (fmt) == "undefined" || fmt === "") {
            fmt = "yyyy-MM-dd"
        } else if (fmt === "date") {
            return date;
        };
        return date.Format(fmt);
    } catch (ex) {
        return "";
    }
}

//日期格式
Date.prototype.Format = function (fmt) { //author: meizz   
    var o = {
        "M+": this.getMonth() + 1, //月份   
        "d+": this.getDate(), //日   
        "h+": this.getHours(), //小时   
        "m+": this.getMinutes(), //分   
        "s+": this.getSeconds(), //秒   
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度   
        "S": this.getMilliseconds() //毫秒   
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

function jsonDateFormatString(jsonDate, fmt) {
    var date = new Date(parseInt(jsonDate.replace("/Date(", "").replace(")/", ""), 10));
    if (date.getFullYear() > 1753) return jsonDateFormat(jsonDate, 'yyyy-MM-dd hh:mm:ss');
    return '';
}

//#endregion


//#region 创建新框架
function createFrame(url) {
    var s = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:99%;"></iframe>';
    return s;
}

//#endregion

//#region 获取页面传值 ?
function GetArgsFromHref(sHref, sArgName) 
{
    var args = sHref.split("?");
    var retval = "";
    if (args[0] == sHref) /*参数为空*/ {
        return retval; /*无需做任何处理*/
    }
    var str = args[1];
    args = str.split("&");
    for (var i = 0; i < args.length; i++) {
        str = args[i];
        var arg = str.split("=");
        if (arg.length <= 1) continue;
        if (arg[0] == sArgName) retval = arg[1];
    }
    return retval;
}




//#endregion


//#region 获取页面传值 #
function GetFromHref(sHref, sArgName) {
    var args = sHref.split("#");
    var retval = "";
    if (args[0] == sHref) /*参数为空*/ {
        return retval; /*无需做任何处理*/
    }
    $.each(args, function (i, v) {
        if (i > 0) {
            var arg = v.split("=");
            if (arg[0] == sArgName) {
                if (arg[1].indexOf('?')>-1) {
                    arg[1] = arg[1].split('?')[0];
                }
                retval = arg[1];
            }
        }
    });
    return retval;
}



//#endregion















