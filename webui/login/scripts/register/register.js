var Vsuccess;
// 滑动验证

$(function () {
    var slider = new SliderUnlock("#slider", {
        successLabelTip: "验证成功"
    }, function () {
        //alert("验证成功");
        Vsuccess = 1;
    });

    $("#agree").on("click",function () {
        var isbool = $(this).attr("checked");
        if (isbool) {
            $("#submit").css("background", "#39A5F0");
        } else {
            $("#submit").css("background", "#808080");
        }
    });

    slider.init();
})
//表单验证
$(function(){
    $('#username').on('blur change',function(){
        var inputlength=$(this).val().length;
        var inputVal=$(this).val();
        var reg=/\s/.test(inputVal);
        var $usernameError=$('.usernameError');
        if(!reg&&inputlength>5&&inputlength<17){
            $usernameError.hide();
         }else{
            $usernameError.show();
         }
    })
    $('#password').on('blur change',function(){
        var inputlength=$(this).val().length;
        var inputVal=$(this).val();
        var reg=/\s/.test(inputVal);
        var $passwordError=$('.passwordError');
         if(!reg&&inputlength>5&&inputlength<17){
            $passwordError.hide();
         }else{
            $passwordError.show();
         }
    })
    $('#confirmPassword').on('blur change',function(){
        var confirmPassword=$(this).val();
        var inputVal=$(this).val();
        var password=$('#password').val();
        var reg=/\s/.test(inputVal);
        var $confirmPasswordError=$('.confirmPasswordError');
         if(!reg&&confirmPassword==password&&inputVal.length>0){
            $confirmPasswordError.hide();
         }else{
            $confirmPasswordError.show();
         }
    })
    $('#email').on('blur change',function(){
        var inputVal=$(this).val();
        var reg=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/.test(inputVal);
        var $emailError=$('.emailError');
         if(reg){
            $emailError.hide();
         }else{
            $emailError.show();
         }
    })
    $('#phoneNum').on('blur change',function(){
        var inputVal = $(this).val();
        var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(inputVal);
        var $phoneNumError=$('.phoneNumError');
         if(reg){
            $phoneNumError.hide();
         }else{
            $phoneNumError.show();
         }
    })


})
function GetPost() {
    var option = {};
    option.UserAccountId = $("#username").val();
    option.UserAccountPassWord = $("#password").val();
    option.UserAccountPhone = $("#phoneNum").val();
    option.UserAccountNickName = $("#lastName").val();
    option.UserAccountEmail = $("#email").val();
    option.UserAccountEnum = $('input[name="workplace"]:checked').val();
    //option.UserAccountCreateDate = jsonDateFormat(new Date(), "yyyy-MM-dd");
    return option;
}

//注册
function Register() {
    var url = "/api/UserAccount/InsertUserAccount";
    var agree = $("#agree").prop("checked");
    var ss = $("#registerForm");
    regGetVal();
    if (agree) {
        if (Vsuccess == 1) {
            var data = jsonPost(url, GetPost());
            if (data) {
                var datas = JSON.parse(data);
                if (datas.Result == 1) {
                    alert(datas.Message);
                    window.location.href = webConfigs.path + "/ssoui/login.html";
                } else {
                    alert(datas.Message);
                }
            }
        } else {
            alert("请拖动滑块验证");
        }
    } else {
        alert("请阅读相关服务条款并选择");
        return;
    }

}

function regGetVal() {

    var inputlength = $('#username').val().length;
    var inputVal = $('#username').val();
    var reg = /\s/.test(inputVal);
    var $usernameError = $('.usernameError');
    if (!reg && inputlength > 5 && inputlength < 17) {
        $usernameError.hide();
    } else {
        $('#username').focus();
        $usernameError.show();
        return;
    }
    

    inputlength = $('#password').val().length;
    inputVal = $('#password').val();
    reg = /\s/.test(inputVal);
    var $passwordError = $('.passwordError');
    if (!reg && inputlength > 5 && inputlength < 17) {
        $passwordError.hide();
    } else {
        $('#password').focus();
        $passwordError.show();
        return;
    }
    

    inputVal = $("#confirmPassword").val();
    password = $('#password').val();
    reg = /\s/.test(inputVal);
    var $confirmPasswordError = $('.confirmPasswordError');
    if (!reg && inputVal == password && inputVal.length > 0) {
        $confirmPasswordError.hide();
    } else {
        $("#confirmPassword").focus();
        $confirmPasswordError.show();
        return;
    }

    inputVal = $('#email').val();
    reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/.test(inputVal);
    var $emailError = $('.emailError');
    if (reg) {
        $emailError.hide();
    } else {
        $('#email').focus();
        $emailError.show();
        return;
    }

    inputVal = $('#phoneNum').val();
    reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(inputVal);
    var $phoneNumError = $('.phoneNumError');
    if (reg) {
        $phoneNumError.hide();
    } else {
        $('#phoneNum').focus();
        $phoneNumError.show();
        return;
    }

}



